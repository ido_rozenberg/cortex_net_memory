import tensorflow as tf
from tensorflow.contrib import slim

from object_detection.utils import shape_utils, static_shape


class Memory(object):
    """Memory module."""

    def __init__(self, memory_size, network_dims, encoding_depth=256):
        memory_shape = list(network_dims) + [memory_size]
        with tf.variable_scope('mem_buffer', reuse=tf.AUTO_REUSE):
            self._buffer = tf.get_variable('mem_buffer', shape=memory_shape,
                                           trainable=False,
                                           initializer=tf.zeros_initializer)
        self._attention_method = self._scaled_dot_product_attention
        self._encoding_dim = encoding_depth

    def remember(self, q):
        with tf.variable_scope('memory'):
            assert_op = tf.assert_equal(static_shape.get_batch_size(q.get_shape()), 1)
            with tf.control_dependencies([assert_op]):
                q = tf.squeeze(q, axis=0)
                context, new_q = self.attention(q, self._buffer)
                with tf.control_dependencies([self._push_to_buffer(new_q)]):
                    return tf.expand_dims(context + q, 0)

    def encode_key(self, k):
        """
        Encodes the k to be feed into the attention block.
        Using Atrous conv for larger receptive field.
        :param k: The key to encode.
        :return: An encoded key
        """
        with tf.variable_scope('key_encoder'):
            k = slim.conv2d(k, self._encoding_dim, 3, rate=2)
            return slim.conv2d(k, self._encoding_dim, 3, rate=2)

    def encode_query(self, q):
        """
        Encodes the query to be feed into the attention block and saved later into memory
        :param q: The query to encode
        :return: An encoded query
        """
        with tf.variable_scope('query_encoder'):
            return slim.conv2d(q, self._encoding_dim, 3, biases_initializer=None)

    def attention(self, q, k):
        with slim.arg_scope([slim.conv2d], biases_initializer=None):
            with tf.variable_scope('attention'):
                k = self.encode_key(k)
                q = self.encode_query(q)
                context = self._attention_method(q, k)
                return slim.conv2d(context, self._encoding_dim, 1), q

    def _scaled_dot_product_attention(self, q, k):
        v = tf.matmul(q, k, transpose_b=True)
        h = tf.shape(k, out_type=tf.float32)[0]
        w = tf.shape(k, out_type=tf.float32)[1]
        scale_factor = tf.sqrt(w * h)
        return tf.matmul(tf.nn.softmax(v / scale_factor, axis=-1), v, transpose_b=True)

    def _push_to_buffer(self, v):
        with tf.variable_scope('update_memory'):
            v = slim.conv2d(v, 1, 1, biases_initializer=None)
            value = tf.concat([v, self._buffer], axis=-1)
            value = value[:, :, :-1]
            return self._buffer.assign(value)

    def clear(self):
        with tf.control_dependencies([tf.Print(True, [], message='Mem buffered cleared', first_n=10,
                                               summarize=1000)]):
            update_op = tf.variables_initializer([self._buffer])
            return update_op

    def get_buffer(self):
        return self._buffer
