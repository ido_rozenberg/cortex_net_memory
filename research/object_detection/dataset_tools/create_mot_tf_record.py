# Copyright 2017 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

r"""Convert raw KITTI detection dataset to TFRecord for object_detection.

Converts KITTI detection dataset to TFRecords with a standard format allowing
  to use this dataset to train object detectors. The raw dataset can be
  downloaded from:
  http://kitti.is.tue.mpg.de/kitti/data_object_image_2.zip.
  http://kitti.is.tue.mpg.de/kitti/data_object_label_2.zip
  Permission can be requested at the main website.

  KITTI detection dataset contains 7481 training images. Using this code with
  the default settings will set aside the first 500 images as a validation set.
  This can be altered using the flags, see details below.

Example usage:
    python object_detection/dataset_tools/create_kitti_tf_record.py \
        --data_dir=/home/user/kitti \
        --output_path=/home/user/kitti.record
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import hashlib
import io
import os, sys
from collections import defaultdict
from glob import glob

import numpy as np
import PIL.Image as pil
import tensorflow as tf

from object_detection.utils import dataset_util
from object_detection.utils import label_map_util
from object_detection.utils.np_box_ops import iou

tf.app.flags.DEFINE_string('data_dir', '/hdd/datasets/mot/detection/', 'Location of root directory for the '
                                                                       'data. Folder structure is assumed to be:'
                                                                       '<data_dir>/train/gt/gt.txt (annotations) and'
                                                                       '<data_dir>/train/img1/data_object_image_2/training/image_2'
                                                                       '(images).')
tf.app.flags.DEFINE_string('output_path', '../records/', 'Path to which TFRecord files'
                                              'will be written. The TFRecord with the training set'
                                              'will be located at: <output_path>_train.tfrecord.'
                                              'And the TFRecord with the validation set will be'
                                              'located at: <output_path>_val.tfrecord')
tf.app.flags.DEFINE_string('classes_to_use', 'person',
                           'Comma separated list of class names that will be'
                           'used. Adding the dontcare class will remove all'
                           'bboxs in the dontcare regions.')
tf.app.flags.DEFINE_string('label_map_path', 'object_detection/data/mot_label_map.pbtxt',
                           'Path to label map proto.')
tf.app.flags.DEFINE_integer('validation_set_size', '500', 'Number of images to'
                                                          'be used as a validation set.')
FLAGS = tf.app.flags.FLAGS

TEST_FOLDER = 'test'
TRAIN_FOLDER = 'train'
IMG_FOLDER = 'img1'
GT_FILE = 'gt/gt.txt'
IMG_FILE_NAME = '%s.jpg'
IMG_FILE_NAME_ZERO_LEN = 6

id_to_class = {'1': 'person',
               '2': 'person',
               '3': 'car',
               '4': 'bicycle',
               '5': 'motorbike',
               '6': 'non_motorized_vehicle',
               '7': 'person',
               '8': 'distractor',
               '9': 'occluder',
               '10': 'occluder_on_ground',
               '11': 'occluder_full',
               '12': 'reflection',
               '-1': 'person'}


def id_to_im_file_name(img_id):
    img_id = str(img_id).zfill(IMG_FILE_NAME_ZERO_LEN)
    return IMG_FILE_NAME % img_id


def file_name_to_id(img_name):
    return int(img_name.split('.')[0])


def convert_kitti_to_tfrecords(data_dir, output_path, classes_to_use,
                               label_map_path):
    """Convert the KITTI detection dataset to TFRecords.

    Args:
      data_dir: The full path to the unzipped folder containing the unzipped data
        from data_object_image_2 and data_object_label_2.zip.
        Folder structure is assumed to be: data_dir/training/label_2 (annotations)
        and data_dir/data_object_image_2/training/image_2 (images).
      output_path: The path to which TFRecord files will be written. The TFRecord
        with the training set will be located at: <output_path>_train.tfrecord
        And the TFRecord with the validation set will be located at:
        <output_path>_val.tfrecord
      classes_to_use: List of strings naming the classes for which data should be
        converted. Use the same names as presented in the KIITI README file.
        Adding dontcare class will remove all other bounding boxes that overlap
        with areas marked as dontcare regions.
      label_map_path: Path to label map proto
      validation_set_size: How many images should be left as the validation set.
        (Ffirst `validation_set_size` examples are selected to be in the
        validation set).
    """
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    label_map_dict = label_map_util.get_label_map_dict(label_map_path)

    train_episode_dirs = glob(os.path.join(data_dir, TRAIN_FOLDER, '*'))
    # test_episode_dirs = glob(os.path.join(data_dir, TEST_FOLDER, '*'))
    episode_dirs = train_episode_dirs  # + test_episode_dirs
    for episode_dir in episode_dirs:
        ann_file = os.path.join(episode_dir, GT_FILE)

        image_dir = os.path.join(episode_dir, IMG_FOLDER)
        episode_name = os.path.basename(episode_dir.rstrip('/'))
        tfrecord_name = os.path.join(output_path, '%s.tfrecord' % episode_name)
        train_writer = tf.python_io.TFRecordWriter(tfrecord_name)

        seq_anno = read_annotation_file(ann_file, classes_to_use)
        images = sorted(tf.gfile.ListDirectory(image_dir))
        for img_name in images:
            annotation_for_image = seq_anno[file_name_to_id(img_name)]

            image_path = os.path.join(image_dir, img_name)

            example = prepare_example(image_path, annotation_for_image, label_map_dict)
            train_writer.write(example.SerializeToString())

        train_writer.close()


def prepare_example(image_path, annotations, label_map_dict):
    """Converts a dictionary with annotations for an image to tf.Example proto.

    Args:
      image_path: The complete path to image.
      annotations: A dictionary representing the annotation of a single object
        that appears in the image.
      label_map_dict: A map from string label names to integer ids.

    Returns:
      example: The converted tf.Example.
    """
    with tf.gfile.GFile(image_path, 'rb') as fid:
        encoded_jpg = fid.read()
    encoded_jpg_io = io.BytesIO(encoded_jpg)
    image = pil.open(encoded_jpg_io)
    image = np.asarray(image)

    key = hashlib.sha256(encoded_jpg).hexdigest()

    width = int(image.shape[1])
    height = int(image.shape[0])
    xmin, ymin = annotations['2d_bbox_left'], annotations['2d_bbox_top']
    xmax, ymax = xmin + annotations['2d_bbox_width'], ymin + annotations['2d_bbox_height']

    xmin_norm = xmin / float(width)
    xmin_norm = np.clip(xmin_norm, 0.0, 1.0)
    ymin_norm = ymin / float(height)
    ymin_norm = np.clip(ymin_norm, 0.0, 1.0)
    xmax_norm = xmax / float(width)
    xmax_norm = np.clip(xmax_norm, 0.0, 1.0)
    ymax_norm = ymax / float(height)
    ymax_norm = np.clip(ymax_norm, 0.0, 1.0)

    difficult_obj = [0] * len(xmin_norm)

    example = tf.train.Example(features=tf.train.Features(feature={
        'image/height': dataset_util.int64_feature(height),
        'image/width': dataset_util.int64_feature(width),
        'image/frame_id': dataset_util.int64_feature(annotations['frame']),
        'image/filename': dataset_util.bytes_feature(image_path.encode('utf8')),
        'image/source_id': dataset_util.bytes_feature(image_path.encode('utf8')),
        'image/key/sha256': dataset_util.bytes_feature(key.encode('utf8')),
        'image/encoded': dataset_util.bytes_feature(encoded_jpg),
        'image/format': dataset_util.bytes_feature('jpg'.encode('utf8')),
        'image/object/track_id': dataset_util.float_list_feature(annotations['o_id']),
        'image/object/bbox/xmin': dataset_util.float_list_feature(xmin_norm),
        'image/object/bbox/xmax': dataset_util.float_list_feature(xmax_norm),
        'image/object/bbox/ymin': dataset_util.float_list_feature(ymin_norm),
        'image/object/bbox/ymax': dataset_util.float_list_feature(ymax_norm),
        'image/object/class/text': dataset_util.bytes_list_feature(
            [x.encode('utf8') for x in annotations['class']]),
        'image/object/class/label': dataset_util.int64_list_feature(
            [label_map_dict[x] for x in annotations['class']]),
        'image/object/difficult': dataset_util.int64_list_feature(difficult_obj),
        'image/object/truncated': dataset_util.float_list_feature(
            annotations['visible'])
    }))

    return example


def filter_annotation(annotations, used_classes):
    """Filters out annotations from the unused classes and dontcare regions.

    Filters out the annotations that belong to classes we do now wish to use and
    (optionally) also removes all boxes that overlap with dontcare regions.

    Args:
      img_all_annotations: A list of annotation dictionaries. See documentation of
        read_annotation_file for more details about the format of the annotations.
      used_classes: A list of strings listing the classes we want to keep, if the
      list contains "dontcare", all bounding boxes with overlapping with dont
      care regions will also be filtered out.

    Returns:
      img_filtered_annotations: A list of annotation dictionaries that have passed
        the filtering.
    """

    if annotations['visible'] < 0.2:
        return False
    if annotations['class'] not in used_classes:
        return False
    if annotations['conf'] == 0:
        return False

    return True


def read_annotation_file(filename, classes_to_use):
    """Reads a KITTI annotation file.

    Converts a KITTI annotation file into a dictionary containing all the
    relevant information.

    Args:
      filename: the path to the annotataion text file.

    Returns:
      anno: A dictionary with the converted annotation information. See annotation
      README file for details on the different fields.
    """
    with open(filename) as f:
        content = f.readlines()
    content = [x.strip().split(',') for x in content]

    anno_dict = defaultdict(lambda: list())
    for line in content:
        frame = int(line[0])
        anno = {}
        anno['frame'] = frame
        anno['o_id'] = int(line[1])
        anno['2d_bbox_left'] = float(line[2])
        anno['2d_bbox_top'] = float(line[3])
        anno['2d_bbox_width'] = float(line[4])
        anno['2d_bbox_height'] = float(line[5])
        anno['conf'] = float(line[6])
        anno['class'] = id_to_class[str(int(line[7]))]
        anno['visible'] = float(line[8])
        if filter_annotation(anno, classes_to_use):
            anno_dict[frame].append(anno)
    result_dict = {}
    for frame_id, frame_ann_list in anno_dict.iteritems():
        anno = {}
        anno['class'] = np.array([x['class'] for x in frame_ann_list])
        anno['visible'] = np.array([float(x['visible']) for x in frame_ann_list])
        anno['2d_bbox_left'] = np.array([float(x['2d_bbox_left']) for x in frame_ann_list])
        anno['2d_bbox_top'] = np.array([float(x['2d_bbox_top']) for x in frame_ann_list])
        anno['2d_bbox_width'] = np.array([float(x['2d_bbox_width']) for x in frame_ann_list])
        anno['2d_bbox_height'] = np.array([float(x['2d_bbox_height']) for x in frame_ann_list])
        anno['conf'] = np.array([float(x['conf']) for x in frame_ann_list])
        anno['o_id'] = np.array([int(x['o_id']) for x in frame_ann_list])
        anno['frame'] = frame_id
        result_dict[frame_id] = anno

    return result_dict


def main(_):
    convert_kitti_to_tfrecords(
        data_dir=FLAGS.data_dir,
        output_path=FLAGS.output_path,
        classes_to_use=FLAGS.classes_to_use.split(','),
        label_map_path=FLAGS.label_map_path)


if __name__ == '__main__':
    tf.app.run()
